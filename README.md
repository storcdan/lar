# Informace
V tomto repozitáři se nacházi projekt, který byl vytvořen pro předemět B3B33LAR.
Všechen kód je napsán pro **Python 2.7**
## Spuštění
Finální implementace se nachází v souboru [slalom.py](./imageTest/slalom.py).
Postup pro spuštění kódu je
```shell script
python2.7 slalom.py -i data.mat
```
nebo 
```shell script
python2.7 slalom.py --input data.mat
```
popřípadně s testovacími daty
```shell script
python2.7 slalom.py --test
```
Každopádně je potřebný jeden z nich.

Pro zapnutí vizulizace je potřeba přidat přepínač
```shell script
python2.7 slalom.py -v -i data.mat
```

## Závislosti
Kód používá knihovny 
* OpenCV (verze 3.4.9)
* scipy
* Numpy


## Zdroje
Repozitář obsahuje táké kódy poskytnuté od vyučujících. 
Všechny soubory, které byly poskytnuty, se nachází ve složce _turtlebot-master_. 
Originalní repozitář se nachází [zde](https://gitlab.fel.cvut.cz/wagnelib/turtlebot).

## Autoři
Samotný projekt byl vytvořen _Davidem Mlčochem_ a _Danielem Štorcem_.
