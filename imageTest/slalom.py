import cv2 as cv
import numpy as np
import sys, getopt
from scipy.io import loadmat
import time


GREEN = 0
RED = 1
BLUE = 2

VIS = False
TEST = False
INPUT_FILE = ""


def load_images(image_paths):
    ret = []

    for img_path in image_paths:
        ret.append(cv.imread(img_path))

    return ret


def convert_depth(pc):
    """Function from turtlebot-master/scripts/show_depth.py. Used only for visual effect."""
    x_range = (-0.8, 0.8)
    y_range = (-0.4, 0.3)
    z_range = (0.45, 3.5)

    pc[np.isnan(pc)] = -10

    # mask out floor points and too high points
    mask = pc[:, :, 1] > y_range[0]
    mask = np.logical_and(mask, pc[:, :, 1] < y_range[1])

    mask = np.logical_and(mask, pc[:, :, 0] > x_range[0])
    mask = np.logical_and(mask, pc[:, :, 0] < x_range[1])

    # mask point too far and too close
    mask = np.logical_and(mask, pc[:, :, 2] > z_range[0])
    mask = np.logical_and(mask, pc[:, :, 2] < z_range[1])

    # empty image
    image = np.zeros(mask.shape)

    # assign depth i.e. distance to image
    image[mask] = np.int8(pc[:, :, 2][mask] / 3.0 * 255)
    im_color = cv.applyColorMap(255 - image.astype(np.uint8), cv.COLORMAP_JET)
    return im_color


def threshold_img(img, color):
    """Thresholding of image by colour"""
    if color == GREEN:  # Good
        low_hsv = 45, 70, 40
        high_hsv = 85, 255, 250
        return cv.inRange(img, low_hsv, high_hsv)
    elif color == BLUE:  # TODO
        low_hsv = 90, 150, 120
        high_hsv = 160, 255, 250
        return cv.inRange(img, low_hsv, high_hsv)
    elif color == RED:  # TODO
        low_hsv_1 = 0, 150, 50
        high_hsv_1 = 10, 255, 230
        mask1 = cv.inRange(img, low_hsv_1, high_hsv_1)

        low_hsv_2 = 120, 200, 50
        high_hsv_2 = 180, 255, 200
        mask2 = cv.inRange(img, low_hsv_2, high_hsv_2)
        # why do we have two masks for red?

        return mask1 | mask2
    else:
        raise ValueError("color not in defined colors")


def filter_contours(cnts):
    """Basic filter for poles, we want only the ones that are higher than taller, and at least have some area"""
    ret = []
    for cnt in cnts[1]:
        area = cv.contourArea(cnt)
        x, y, w, h = cv.boundingRect(cnt)
        if area < 1000 or h < 3 * w:
            continue

        ret.append(cnt)

    return ret


def get_bounding_rectangles(cnts):
    ret = []
    for cnt in cnts:
        ret.append(cv.boundingRect(cnt))

    return ret


def get_pole_info(rect, depth_map):
    """Returns the middle of the pole and the depth"""
    x, y, w, h = rect

    dx = int(round(x + w / 2))  # middle
    dy = int(round(y + h / 2))
    pole_depth = depth_map[dy - 1:dy + 2, dx - 1:dx + 2]
    pole_depth = np.nan_to_num(pole_depth)
    pole_depth = pole_depth[pole_depth != 0]
    if len(pole_depth) == 0:
        depth = 9999999     # Not nice
    else:
        depth = np.nanmean(pole_depth)

    return dx, dy, depth


def draw_bounding_rects(img, rects_b=[], rects_g=[], rects_r=[]):
    img1 = img.copy()
    if rects_r:
        img1 = cv.rectangle(img1, (rects_r[0][0], rects_r[0][1]),
                            (rects_r[0][0] + rects_r[0][2], rects_r[0][1] + rects_r[0][3]), (255, 255, 255), 2)
        img1 = cv.rectangle(img1, (rects_r[1][0], rects_r[1][1]),
                            (rects_r[1][0] + rects_r[1][2], rects_r[1][1] + rects_r[1][3]), (255, 255, 255), 2)
    if rects_b:
        img1 = cv.rectangle(img1, (rects_b[0][0], rects_b[0][1]),
                            (rects_b[0][0] + rects_b[0][2], rects_b[0][1] + rects_b[0][3]), (255, 0, 0), 2)
        img1 = cv.rectangle(img1, (rects_b[1][0], rects_b[1][1]),
                            (rects_b[1][0] + rects_b[1][2], rects_b[1][1] + rects_b[1][3]), (255, 0, 0), 2)
    if rects_g:
        print(rects_g)
        img1 = cv.rectangle(img1, (rects_g[0][0], rects_g[0][1]),
                            (rects_g[0][0] + rects_g[0][2], rects_g[0][1] + rects_g[0][3]), (0, 255, 255), 2)
        img1 = cv.rectangle(img1, (rects_g[1][0], rects_g[1][1]),
                            (rects_g[1][0] + rects_g[1][2], rects_g[1][1] + rects_g[1][3]), (0, 255, 255), 2)
    return img1


def map_to_range(in_min, in_max, out_min, out_max, value):
    # type: (int, int, int, int, int) -> float
    return out_min + (float(out_max - out_min) / float(in_max - in_min)) * (value - in_min)


def get_real_world_cords(info, inverse_k_matrix):
    u, v, depth = info
    res = np.dot(depth * inverse_k_matrix, np.array([u, v, 1]))
    return res


def map_coordinates(point):
    point = np.nan_to_num(point)
    dx = int(map_to_range(-2000, 2000, 0, 640, int(point[0])))
    dz = int(map_to_range(0, 3000, 480, 0, int(point[2])))
    # it's necessaary to have correct coordinates ratios in order to have corretc angles in the picture
    return dx, dz


def draw_poles(poles, color, pole_map):
    dx1, dz1 = map_coordinates(poles[0])
    dx2, dz2 = map_coordinates(poles[1])
    cv.circle(pole_map, (int(dx1), int(dz1)), 4, color, -1)
    cv.circle(pole_map, (int(dx2), int(dz2)), 4, color, -1)

    cv.line(pole_map, (int(dx1), int(dz1)), (int(dx2), int(dz2)), (0, 0, 0), 1)
    s = [(dz1 - dz2), -(dx1 - dx2)]
    middle = ((dx1 + dx2) / 2, (dz1 + dz2) / 2)
    lamda = (479 - middle[1]) / float(s[1])  # TODO: Better name, lambda cant be used but wrong spelling isn't an answer

    cv.line(pole_map, (int(middle[0] + lamda * s[0]), 480), middle, (0, 0, 0), 1)

    return pole_map


def get_map(rects, color, depth_map, inverse_k_matrix, cone_map=None):  # rect [x_pixel, y_pixel, width, height]
    p1 = get_pole_info(rects[0], depth_map)
    p2 = get_pole_info(rects[1], depth_map)
    poles = get_real_world_cords(p1, inverse_k_matrix), get_real_world_cords(p2, inverse_k_matrix)

    c_m_w = 640
    c_m_h = 480
    if cone_map is None:
        cone_map = np.zeros((c_m_h, c_m_w, 3), np.uint8)
        cone_map.fill(255)
    cone_map = draw_poles(poles, color, cone_map)
    cv.line(cone_map, (320, 0), (320, 480), (0, 0, 0), 2)
    xm, zm = map_coordinates(((poles[0][0] + poles[1][0]) / 2, 0, (poles[0][2] + poles[1][2]) / 2))
    cv.line(cone_map, (320, 480), (xm, zm), (0, 0, 0), 1)

    return cone_map


def draw_results_on_map(cone_map, results):
    y = 40
    cv.putText(cone_map, "{}".format(INPUT_FILE, 4), (0, 20), cv.FONT_ITALIC, 0.5, (0, 0, 0), 1)
    y += 20
    for key in results:
        cv.putText(cone_map, "{}: {}".format(key, results[key], 4), (420, y), cv.FONT_ITALIC, 0.5, (0, 0, 0), 1)
        y += 20


def get_final_rect(rects_r, rects_g, rects_b, depth_cloud, img):
    all_rects = rects_r + rects_g + rects_b
    poles_depth = []
    for rect in all_rects:
        poles_depth.append(get_pole_info(rect, depth_cloud)[2])

    for i in range(len(poles_depth)):  # check if depth is valid
        if poles_depth[i] <= 350:
            poles_depth[i] = 4000.0

    rect1_depth = min(poles_depth)
    rect1_index = poles_depth.index(rect1_depth)
    poles_depth[rect1_index] = 4000
    rect2_depth = min(poles_depth)
    rect2_index = poles_depth.index(rect2_depth)
    final_rects = [all_rects[rect1_index], all_rects[rect2_index]]

    if rect1_index >= len(rects_r) + len(rects_g) and rect2_index >= len(rects_r) + len(rects_g):
        color = 'Blue'
        img = draw_bounding_rects(img, rects_b=final_rects)
    elif rect1_index >= len(rects_r) and rect2_index >= len(rects_r):
        color = 'Green'
        img = draw_bounding_rects(img, rects_g=final_rects)
    else:
        color = 'Red'
        img = draw_bounding_rects(img, rects_r=final_rects)
    return final_rects, color, img


def cg(rects, img):
    x1, y1, w1, h1 = rects[0]
    x2, y2, w2, h2 = rects[1]

    g = abs(x2 - x1)
    if x2 > x1:
        g = g - w1
        c = (x1 + w1 + g / 2) - 320
    else:
        g = g - w2
        c = (x2 + w2 + g / 2) - 320

    ym = (y1 + h1 / 2 + y2 + h2 / 2) / 2

    img1 = img.copy()
    cv.line(img1, (320 + c - g / 2, ym), (320 + c + g / 2, ym), (255, 255, 255), 4)
    cv.circle(img1, (int(320 + c), ym), 3, (0, 255, 255), 6)

    return c, g, img1


def xz_distance(points):
    """Returns distance in meters"""
    dist = np.sqrt((points[0][0] - points[1][0]) ** 2 + (points[0][2] - points[1][2]) ** 2)
    return dist / 1000


def middle_distance(points):
    x = (points[0][0] + points[1][0]) / 2  # x,z = coordinates of the middle of the gate
    z = (points[0][2] + points[1][2]) / 2
    dist = xz_distance([(0, 0, 0), (x, 0, z)])
    return dist


def alpha_deg(points):
    x = (points[0][0] + points[1][0]) / 2.0  # x,z = coordinates of the middle of the gate
    z = (points[0][2] + points[1][2]) / 2.0
    alpha = np.arctan(x / z)
    return alpha


def beta_deg(points, alpha):
    if (points[1][2] - points[0][2]) != 0:
        beta = np.pi / 2 - abs(alpha) - abs(
            np.arctan((points[1][0] - points[0][0]) / float((points[1][2] - points[0][2]))))
        if (points[1][0] - points[0][0]) / float(
                (points[1][2] - points[0][2])) < 0:  # different orientation of the gate
            beta = - beta
    else:
        beta = - alpha

    return beta


def task3(filename):
    data = loadmat(filename)

    img = np.asarray(data["image_rgb"], np.uint8)
    inverse_k_matrix = np.linalg.inv(data["K_rgb"])

    img_hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)

    depth_cloud = np.asarray(data["image_depth"])
    deep_img = np.asarray(data["point_cloud"])
    deep_img = convert_depth(deep_img)

    mask_r = threshold_img(img_hsv, RED)
    mask_g = threshold_img(img_hsv, GREEN)
    mask_b = threshold_img(img_hsv, BLUE)

    threshold_img_r = cv.bitwise_and(img, img, mask=mask_r)
    threshold_img_g = cv.bitwise_and(img, img, mask=mask_g)
    threshold_img_b = cv.bitwise_and(img, img, mask=mask_b)

    threshold_deep_img_r = cv.bitwise_and(deep_img, deep_img, mask=mask_r)
    threshold_deep_img_g = cv.bitwise_and(deep_img, deep_img, mask=mask_g)
    threshold_deep_img_b = cv.bitwise_and(deep_img, deep_img, mask=mask_b)

    contours_r = cv.findContours(mask_r, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
    contours_g = cv.findContours(mask_g, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
    contours_b = cv.findContours(mask_b, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)

    contours_r = filter_contours(contours_r)
    contours_g = filter_contours(contours_g)
    contours_b = filter_contours(contours_b)

    if VIS:
        cv.imshow("Images", np.vstack((np.hstack((img, threshold_img_g, threshold_img_r, threshold_img_b)), np.hstack(
            (deep_img, threshold_deep_img_g, threshold_deep_img_r, threshold_deep_img_b)))))

    rects_r = get_bounding_rectangles(contours_r)
    rects_g = get_bounding_rectangles(contours_g)
    rects_b = get_bounding_rectangles(contours_b)

    final_rects, color, img = get_final_rect(rects_r, rects_g, rects_b, depth_cloud, img)

    p1 = get_pole_info(final_rects[0], depth_cloud)  # middle points of poles
    p2 = get_pole_info(final_rects[1], depth_cloud)

    poles = get_real_world_cords(p1, inverse_k_matrix), get_real_world_cords(p2, inverse_k_matrix)

    c, g, img = cg(final_rects, img)

    d = xz_distance(poles) - 0.05  # 0.05 = width of the pole
    v = middle_distance(poles)
    alpha = alpha_deg(poles)
    beta = beta_deg(poles, alpha)

    if d * np.cos(beta) > 0.40:  # 0.40 = width of the robot
        gothrough = True
    else:
        gothrough = False

    result = {'Color': color, 'GoThrough': gothrough, 'c': c, 'g': g, 'Alpha': alpha, 'Beta': beta, 'd': d, 'v': v}

    cone_map = get_map(final_rects, (0, 255, 0), depth_cloud, inverse_k_matrix)
    draw_results_on_map(cone_map, result)
    if VIS:
        cv.imshow("Result", np.hstack((img, cone_map)))

    return result


def load_files():
    ret = []
    # TASK3
    filenames = ['photos/task3/set_S01/2020-04-23-10-37-53.mat',
                 'photos/task3/set_S01/2020-04-23-10-38-09.mat',
                 'photos/task3/set_S01/2020-04-23-10-38-40.mat',
                 'photos/task3/set_S01/2020-04-23-10-38-49.mat',
                 'photos/task3/set_S01/2020-04-23-10-39-03.mat']
    ret.append(filenames)

    filenames = ['photos/task3/set_S02/2020-04-23-10-43-24.mat',
                 'photos/task3/set_S02/2020-04-23-10-43-34.mat',
                 'photos/task3/set_S02/2020-04-23-10-44-05.mat',
                 'photos/task3/set_S02/2020-04-23-10-44-34.mat',
                 'photos/task3/set_S02/2020-04-23-10-44-47.mat']
    ret.append(filenames)

    filenames = ['photos/task3/set_S03/2020-04-23-10-52-19.mat',
                 'photos/task3/set_S03/2020-04-23-10-52-37.mat',
                 'photos/task3/set_S03/2020-04-23-10-52-52.mat',
                 'photos/task3/set_S03/2020-04-23-10-53-11.mat',
                 'photos/task3/set_S03/2020-04-23-10-53-20.mat',
                 'photos/task3/set_S03/2020-04-23-10-53-34.mat']
    ret.append(filenames)

    filenames = ['photos/task3/set_S04/2020-04-23-10-57-17.mat',
                 'photos/task3/set_S04/2020-04-23-10-57-27.mat',
                 'photos/task3/set_S04/2020-04-23-10-57-34.mat',
                 'photos/task3/set_S04/2020-04-23-10-57-50.mat',
                 'photos/task3/set_S04/2020-04-23-10-58-03.mat',
                 'photos/task3/set_S04/2020-04-23-10-58-17.mat']
    ret.append(filenames)

    return ret


def parse_arguments():
    global VIS
    global TEST
    global INPUT_FILE

    argv = sys.argv[1:]
    opts, args = None, None
    try:
        opts, args = getopt.getopt(argv, "hi:v", ["input=", "test", "help"])
    except getopt.GetoptError:
        print "Bad usage, use -h or --help for info"
    for opt, arg in opts:
        if opt in ["-h", "--help"]:
            print("Usage:\n",
                  "-i (--input) <input .mat file>, mandatory if --test not used\n",
                  "-v, show visualisation"
                  "--test, load test files, mainly used for debug\n",
                  "-h (--help), display this help")
        elif opt == "-v":
            VIS = True
        elif opt in ["-i", "--input"]:
            INPUT_FILE = arg
        elif opt in "--test":
            TEST = True


def main():
    global INPUT_FILE
    parse_arguments()
    if TEST:
        test_list = load_files()
    else:
        if INPUT_FILE == "":
            print("Error input file not set")
            exit(1)
        test_list = [[INPUT_FILE]]
    key = None

    for test_case in test_list:
        results = []
        result = []
        for filename in test_case:
            INPUT_FILE = filename
            print ("Processing file {}".format(filename))
            result = task3(filename)
            results.append(filename)

            print("The color of the nearest gate: {}\n"
                  "Robot will go through the gate: {}\n"
                  "c (pixels): {}\n"
                  "g (pixels): {}\n"
                  "Alpha (rad): {}\n"
                  "Beta (rad): {}\n"
                  "d (m): {}\n"
                  "v (m): {}\n".format(result['Color'], result['GoThrough'],
                                       result['c'], result['g'], result['Alpha'],
                                       result['Beta'], result['d'], result['v']))
            if VIS:
                key = cv.waitKey(0)

    while key != 113 and VIS:
        key = cv.waitKey(0)


if __name__ == '__main__':
    main()
