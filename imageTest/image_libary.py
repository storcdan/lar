"""
Testing file where we tried everything fist, no actual use but basic for future tasks
"""



import cv2 as cv
import numpy as np
import sys
from scipy.io import loadmat
import time

GREEN = 0
RED = 1
BLUE = 2

inverse_k_matrix = None

REAL_MIN_X = -1500
REAL_MAX_X = 1500
REAL_MIN_Y = 0
REAL_MAX_Y = 5000

MAP_WIDTH = 640
MAP_HEIGHT = 480


def load_images(image_paths):
    ret = []

    for img_path in image_paths:
        ret.append(cv.imread(img_path))

    return ret


def convert_depth(pc):
    x_range = (-0.3, 0.3)
    z_range = (0.3, 4.0)
    # mask out floor points
    mask = pc[:, :, 1] > x_range[0]

    # mask point too far and close
    mask = np.logical_and(mask, pc[:, :, 2] > z_range[0])
    # mask = np.logical_and(mask, pc[:, :, 2] < z_range[1])

    # empty image
    image = np.zeros(mask.shape)

    # assign depth i.e. distance to image
    image[mask] = np.int8(pc[:, :, 2][mask] / 3.0 * 255)
    im_color = cv.applyColorMap(255 - image.astype(np.uint8), cv.COLORMAP_JET)
    return im_color


def threshold_img(img, color):
    if color == GREEN:  # Good
        low_hsv = 45, 100, 30
        high_hsv = 85, 255, 220
        return cv.inRange(img, low_hsv, high_hsv)
    elif color == BLUE:  # TODO
        low_hsv = 90, 150, 120
        high_hsv = 160, 255, 250
        return cv.inRange(img, low_hsv, high_hsv)
    elif color == RED:  # TODO
        low_hsv_1 = 0, 200, 50
        high_hsv_1 = 10, 255, 200
        mask1 = cv.inRange(img, low_hsv_1, high_hsv_1)

        low_hsv_2 = 120, 200, 50
        high_hsv_2 = 180, 255, 200
        mask2 = cv.inRange(img, low_hsv_2, high_hsv_2)

        return mask1 | mask2
    else:
        raise ValueError("color not in defined colors")


def filter_contours(cnts):
    ret = []
    for cnt in cnts[1]:
        area = cv.contourArea(cnt)
        x, y, w, h = cv.boundingRect(cnt)
        area2 = h * w

        if area < 50 or h < 5 * w:
            continue
        ret.append(cnt)

    return ret


def get_bounding_rectangles(cnts):
    ret = []
    for cnt in cnts:
        ret.append(cv.boundingRect(cnt))

    return ret


def get_pole_info(rect, depth_map):
    x, y, w, h = rect

    dx = int(round(x + w / 2))
    dy = int(round(y + h / 2))

    return dx, dy, depth_map[dy][dx]


def draw_bounding_rects(img, rects_b, rects_g, rects_r):
    img = cv.rectangle(img, (rects_r[0][0], rects_r[0][1]),
                       (rects_r[0][0] + rects_r[0][2], rects_r[0][1] + rects_r[0][3]), (255, 255, 255), 2)
    img = cv.rectangle(img, (rects_r[1][0], rects_r[1][1]),
                       (rects_r[1][0] + rects_r[1][2], rects_r[1][1] + rects_r[1][3]), (255, 255, 255), 2)

    img = cv.rectangle(img, (rects_b[0][0], rects_b[0][1]),
                       (rects_b[0][0] + rects_b[0][2], rects_b[0][1] + rects_b[0][3]), (255, 0, 0), 2)
    img = cv.rectangle(img, (rects_b[1][0], rects_b[1][1]),
                       (rects_b[1][0] + rects_b[1][2], rects_b[1][1] + rects_b[1][3]), (255, 0, 0), 2)

    img = cv.rectangle(img, (rects_g[0][0], rects_g[0][1]),
                       (rects_g[0][0] + rects_g[0][2], rects_g[0][1] + rects_g[0][3]), (0, 255, 255), 2)
    img = cv.rectangle(img, (rects_g[1][0], rects_g[1][1]),
                       (rects_g[1][0] + rects_g[1][2], rects_g[1][1] + rects_g[1][3]), (0, 255, 255), 2)

    return img


def map_to_range(in_min, in_max, out_min, out_max, value):
    # type: (int, int, int, int, int) -> float
    return out_min + (float(out_max - out_min) / float(in_max - in_min)) * (value - in_min)


def get_real_world_cords(info):
    u, v, depth = info

    # hack for develop TODO: remove
    if depth == 0:
        print("Fuck")
        depth = 4000

    res = np.dot(depth * inverse_k_matrix, np.array([u, v, 1]))
    return res


def draw_pole(pole, color, pole_map):
    dx = map_to_range(-1500, 1500, 0, MAP_WIDTH, int(pole[0]))
    dz = map_to_range(0, 5000, MAP_HEIGHT, 0, int(pole[2]))
    cv.circle(pole_map, (int(dx), int(dz)), 3, color, -1)
    # cv.line(pole_map,(250, 480), (int(dx), int(dz)), color, 1)

    return pole_map


def get_map(blue_rect, green_rect, red_rect, depth_map):
    b1 = get_pole_info(blue_rect[0], depth_map)
    b2 = get_pole_info(blue_rect[1], depth_map)
    g1 = get_pole_info(green_rect[0], depth_map)
    g2 = get_pole_info(green_rect[1], depth_map)
    r1 = get_pole_info(red_rect[0], depth_map)
    r2 = get_pole_info(red_rect[1], depth_map)

    green_poles = get_real_world_cords(g1), get_real_world_cords(g2)
    red_poles = get_real_world_cords(r1), get_real_world_cords(r2)
    blue_poles = get_real_world_cords(b1), get_real_world_cords(b2)

    cone_map = np.zeros((MAP_HEIGHT, MAP_WIDTH, 3), np.uint8)
    cone_map.fill(255)

    cone_map = draw_pole(green_poles[0], (0, 255, 0), cone_map)
    cone_map = draw_pole(green_poles[1], (0, 255, 0), cone_map)

    cone_map = draw_pole(red_poles[0], (0, 0, 255), cone_map)
    cone_map = draw_pole(red_poles[1], (0, 0, 255), cone_map)

    cone_map = draw_pole(blue_poles[0], (255, 0, 0), cone_map)
    cone_map = draw_pole(blue_poles[1], (255, 0, 0), cone_map)

    cone_map = create_basic_trajectory([green_poles, red_poles, blue_poles], cone_map)

    return cone_map


def create_basic_trajectory(pair_of_poles, pole_map):
    start_point = int(MAP_WIDTH/2), MAP_HEIGHT

    for pole in pair_of_poles:
        mid_point = int(float(pole[0][0] + pole[1][0]) / 2), int(float(pole[0][2] + pole[1][2]) / 2)
        end_point = int(map_to_range(REAL_MIN_X, REAL_MAX_X, 0, MAP_WIDTH, mid_point[0])), \
                 int(map_to_range(REAL_MIN_Y, REAL_MAX_Y, MAP_HEIGHT, 0, mid_point[1]))
        cv.line(pole_map, start_point, end_point, (0, 0,0), 3)
        start_point = end_point

    return pole_map


if __name__ == '__main__':
    datas = []
    datas.append(loadmat("photos/2020-03-05-15-13-36.mat"))
    # datas.append(loadmat("photos/2020-03-05-15-15-31.mat"))
    for data in datas:
        img = np.asarray(data["image_rgb"], np.uint8)
        inverse_k_matrix = np.linalg.inv(data["K_rgb"])

        img_hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)

        depth_cloud = np.asarray(data["image_depth"])
        deep_img = np.asarray(data["point_cloud"])
        deep_img = convert_depth(deep_img)

        mask_b = threshold_img(img_hsv, BLUE)
        threshold_img_b = cv.bitwise_and(img, img, mask=mask_b)
        threshold_deep_img_b = cv.bitwise_and(deep_img, deep_img, mask=mask_b)

        mask_g = threshold_img(img_hsv, GREEN)
        threshold_img_g = cv.bitwise_and(img, img, mask=mask_g)
        threshold_deep_img_g = cv.bitwise_and(deep_img, deep_img, mask=mask_g)

        mask_r = threshold_img(img_hsv, RED)
        threshold_img_r = cv.bitwise_and(img, img, mask=mask_r)
        threshold_deep_img_r = cv.bitwise_and(deep_img, deep_img, mask=mask_r)
        x1 = cv.connectedComponentsWithStats(mask_g.astype(np.uint8))

        contours_g = cv.findContours(mask_g, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
        contours_g = filter_contours(contours_g)
        contours_r = cv.findContours(mask_r, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
        contours_r = filter_contours(contours_r)
        contours_b = cv.findContours(mask_b, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
        contours_b = filter_contours(contours_b)

        img = cv.imread("test.png")
        cv.imshow("Test", np.vstack((np.hstack((img, threshold_img_g, threshold_img_r, threshold_img_b)), np.hstack(
            (deep_img, threshold_deep_img_g, threshold_deep_img_r, threshold_deep_img_b)))))

        rects_g = get_bounding_rectangles(contours_g)
        rects_b = get_bounding_rectangles(contours_b)
        rects_r = get_bounding_rectangles(contours_r)

        img = draw_bounding_rects(img, rects_b, rects_g, rects_r)
        cone_map = get_map(rects_b, rects_g, rects_r, depth_cloud)

        cv.imshow("Result {}".format(datas.index(data)), np.hstack((img, cone_map)))

    key = cv.waitKey(0)
    while key != 113:
        key = cv.waitKey(0)

